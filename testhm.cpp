#include <iostream>
#include <string>
#include "gtest/gtest.h"
#include "hushmup.h"

using namespace std;

int hash(int _key) { return _key; }

int hash(string _key){
    int hashed = 0;
    for(unsigned int x = 0; x < _key.size(); ++x) {
        hashed += _key.at(x);
    }
    return hashed;
}

template <typename R>
R hash(R _key) { return _key;}

TEST(hushmup, empty) {
    int (*hush)(string);
    hush = hash;
    HushMup<string, string> *mup = new HushMup<string, string>(hush);
    ASSERT_EQ(mup->size(), 0) 
        << "HushMup should be empty when created";
    delete mup;
}

TEST(hushmup, hash1) {
    int (*hush)(int);
    hush = hash;
    ASSERT_EQ((*hush)(120), 120) << "ints not equal";
}

TEST(hushmup, hash2) {
    int (*hush)(string);
    hush = hash;
    string l = "lullaby";
    string i = "lull";
    ASSERT_NE((*hush)("lullaby"), (*hush)(i)) << "strings not equal";
}

TEST(hushmup, identity_hashing) {
    string (*hush)(string);
    hush = hash;
    ASSERT_EQ((*hush)("lullaby"), "lullaby") << "strings not equal";
}

TEST(hushmup, insertion1) {
    int (*hush)(int);
    hush = hash;
    HushMup<int, int> *mup = new HushMup<int, int>(hush);
    int a = 2;
    int b = 5;
    mup->put(a, b);
    ASSERT_EQ(mup->size(), 1);
    ASSERT_EQ(mup->has(a), true) << "should have 2";
    ASSERT_EQ(mup->get(a), b);
    delete mup;
}

TEST(hushmup, insertion_same1){
    int (*hush)(int);
    hush = hash;
    HushMup<int, int> *mup = new HushMup<int, int>(hush);
    int a = 2;
    int b = 5;
    mup->put(a, b);
    mup->put(a, b);
    mup->put(a, b);
    ASSERT_EQ(mup->size(), 1);
    ASSERT_EQ(mup->has(a), true) << "should have 2";
    ASSERT_EQ(mup->get(a), b);
    delete mup;
}

TEST(hushmup, insertion_multiple1) {
    int (*hush)(int);
    hush = hash;
    HushMup<int, int> *mup = new HushMup<int, int>(hush);
    int a = 2;
    int b = 5;
    mup->put(a, b);
    mup->put(a, b);
    ASSERT_EQ(mup->size(), 1);
    int c = 4;
    int d = 5;
    mup->put(c, d);
    ASSERT_EQ(mup->size(), 2);
    mup->put(c, d);
    mup->put(c, d);
    ASSERT_EQ(mup->size(), 2);
    ASSERT_EQ(mup->has(c), true);
    ASSERT_EQ(mup->get(c), d);
    delete mup;
}

TEST(hushmup, insertion_grow1) {
    int (*hush)(int);
    hush = hash;
    HushMup<int, int> mup(hush);
    int a = 2; int b = 3;  int m =  12; int s = 8763;
    int c = 4; int d = 5;  int n =  324; int t = 876;
    int e = 6; int f = 7;  int o = 645; int u = 75;
    int g = 8; int h = 9;  int p =  76; int v = 46;
    int i = 10; int j = 11; int q = 867; int w = 43;
    int k = 24; int l = 32; int r = 23; int x = 456;
    mup.put(a, b); mup.put(c, d);
    ASSERT_EQ(mup.size(), 2);
    mup.put(e, f); mup.put(g, h);
    ASSERT_EQ(mup.size(), 4);
    mup.put(g, h); mup.put(i, j);
    ASSERT_EQ(mup.size(), 5);
    mup.put(a, a); mup.put(m, n);
    ASSERT_EQ(mup.size(), 6);
    mup.put(o, p); mup.put(q, r);
    ASSERT_EQ(mup.size(), 8);
    mup.put(s, t); mup.put(u, v);
    ASSERT_EQ(mup.size(), 10);
    mup.put(w, x); mup.put(k, l);
    ASSERT_EQ(mup.size(), 12);
//  delete mup;
}

TEST(hushmup, insertion2) {
    int (*hush)(string);
    hush = hash;
    HushMup<string, double> *mup = new HushMup<string, double>(hush);
    string a = "jenna";
    double b = 2.3;
    mup->put(a, b);
    ASSERT_EQ(mup->size(), 1);
    ASSERT_EQ(mup->has(a), true) << "should have jenna";
    delete mup;
}

TEST(hushmup, insertion_same2){
    int (*hush)(string);
    hush = hash;
    HushMup<string, double> *mup = new HushMup<string, double>(hush);
    string a = "jenna";
    double b = 2.3;
    mup->put(a, b);
    mup->put(a, b);
    mup->put(a, b);
    ASSERT_EQ(mup->size(), 1);
    ASSERT_EQ(mup->has(a), true);
    ASSERT_EQ(mup->get(a), b);
    delete mup;
}

TEST(hushmup, insertion_multiple2) {
    int (*hush)(string);
    hush = hash;
    HushMup<string, double> *mup = new HushMup<string, double>(hush);
    string a = "jenna";
    double b = 2.3;
    mup->put(a, b);
    mup->put(a, b);
    ASSERT_EQ(mup->size(), 1);
    string c = "Life and Love";
    double d = 3.2;
    mup->put(c, d);
    ASSERT_EQ(mup->size(), 2);
    mup->put(c, d);
    mup->put(c, d);
    ASSERT_EQ(mup->size(), 2);
    ASSERT_EQ(mup->has(c), true);
    ASSERT_EQ(mup->get(c), d);
    delete mup;
}

TEST(hushmup, insertion_grow2) {
    int (*hush)(string);
    hush = hash;
    HushMup<string, double> *mup = new HushMup<string, double>(hush);
    string a = "Duncan"; double k = 22.22;
    string b = "Erica"; double l = 33.33;
    string c = "Chad"; double m = 44.44;
    string d = "Chris"; double n = 55.55;
    string e = "Hayden"; double o = 66.66;
    string f = "Tierney"; double p = 77.77;
    string g = "Dr. Josh Eckroth"; double q = 88.88;
    string h = "Josue"; double r = 99.99;
    string i = "Dr. Tom Vogel"; double s = 12.12;
    string j = "Dr. Dan Plante"; double t = 13.13;
    string u = "Grateful"; double v = 14.14;
    mup->put(a, k); mup->put(b, l);
    ASSERT_EQ(mup->size(), 2);
    mup->put(d, n); mup->put(f, m);
    ASSERT_EQ(mup->size(), 4);
    mup->put(g, o); mup->put(i, p);
    ASSERT_EQ(mup->size(), 6);
    mup->put(a, r); mup->put(j, q);
    ASSERT_EQ(mup->size(), 7);
    mup->put(c, s); mup->put(h, t);
    ASSERT_EQ(mup->size(), 9);
    mup->put(e, o); mup->put(u, v);
    ASSERT_EQ(mup->size(), 11);
    delete mup;
}

//TEST(hushmup, duplicate_insertion) {
//    int (*hush)(int);
//    hush = hash;
//    HushMup<int, int> *mup = new HushMup<int, int>(hush);
//    delete mup;
//}

//TEST(hushmup, ) {
//    int (*hush)();
//    hush = hash;
//    HushMup<,> *mup = new HushMup<,>(hush);
//    delete mup;
//}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
  
