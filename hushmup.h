#ifndef HUSHMUP
#define HUSHMUP

#include <string>
#include <iostream>
#include <cassert>
#include <fstream>
using namespace std;

template <typename T, typename S>
class HushMup {
    private:
        T *key;
        S *val;
        bool *occupied;
        int total, capacity, (*hash_ptr)(T);
        void grow();

    public:

        HushMup(int (*func_ptr)(T));
        void put(T& _key, S& _val); 
        bool has(T& _key);
        S get(T& _key);
        void del(T& _key);

        int size() { return total; }

        ~HushMup();
};

template <typename T, typename S>
HushMup<T,S>::HushMup(int (*func_ptr)(T)){
    capacity = 10;
    total = 0;
    hash_ptr = func_ptr;
    key = new T[capacity];
    val = new S[capacity];
    occupied = new bool[capacity];
    for(int x = 0; x < capacity; ++x) {
        occupied[x] = false;
    }
}

template <typename T, typename S>
void HushMup<T,S>::put(T& _key, S& _val) {
    if(!has(_key)) {
        int location = (*hash_ptr)(_key);
        int loc = location % capacity;
        if(loc >= capacity && total >= capacity)
            grow();
        if(occupied[loc] == false) {
            key[loc] = _key;
            val[loc] = _val;
            occupied[loc] = true;
            ++total;
        }
        else {
            int x = loc;
            while(occupied[x]) {
                x++;
                if(x == capacity)
                    x = 0;
                else if ( x == loc){
                    x = capacity;
                    this->grow();
                    break;
                }
            }
            if ( occupied[x] == false){
                key[x] = _key;
                val[x] = _val;
                occupied[x] = true;
                ++total;
            }
        }
    }
}

template <typename T, typename S>
bool HushMup<T,S>::has(T& _key) {
    for(int x = 0; x < capacity; ++x){
        if(key[x] == _key && occupied[x] == true)
            return true;
        else if (key[x] == _key && occupied[x] == false)
            break;
    }
    return false;
}

template <typename T, typename S>
S HushMup<T,S>::get(T& _key) {
    assert(has(_key));
    S value;
    int loc = (*hash_ptr)(_key);
    int x = loc % capacity;
    while(key[x] != _key) {
        x++;
        if(x == capacity)
            x = 0;
    }
    if( key[x] == _key && occupied[x] == true)
        value = val[x];
    return value;
}

template <typename T, typename S>
void HushMup<T,S>::grow(){
    int newcapacity = capacity * 2;
    T* newKey = new T[newcapacity];
    S* newVal = new S[newcapacity];
    bool* newOcc = new bool[newcapacity];
    for (int x = 0; x < capacity; ++x) {
        newKey[x] = key[x];
        newVal[x] = val[x];
        newOcc[x] = occupied[x];
    }
    for(int x = capacity; x < newcapacity; ++x) {
        newOcc[x] = false;
    }
    delete key;
    delete val;
    delete occupied;
    capacity = newcapacity;
    key = newKey;
    occupied = newOcc;
    val = newVal;
    delete[] newKey; delete[] newVal;
    delete[] newOcc;
}

template <typename T, typename S>
void HushMup<T,S>::del(T& _key) {
    assert(has(_key));
    int loc = (*hash_ptr)(_key);
    int x = loc % capacity;
    while(key[x] != _key) {
        x++;
        if(x == capacity)
            x = 0;
    }
    if(key[x] == _key && occupied[x] == true) {
        occupied[x] = false;
        --total;
    }
}

template <typename T, typename S>
HushMup<T,S>::~HushMup() {
    delete[] key;
    delete[] val;
    delete[] occupied;
}

#endif
